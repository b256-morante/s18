/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.
*/
		
		function checkSum(num, numb) {

		let sumAns = num + numb;
		console.log("Displayed sum of " + num + " and " +numb + " is ");

		console.log(sumAns);

	}

	checkSum(15, 5);
	
/*
		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

		function checkDiff(number, numbers) {

		let diffAns = number - numbers;
		console.log("Displayed difference of " + number + " and " +numbers + " is ");

	
		console.log(diffAns);

	}

	checkDiff(20, 5);



/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

*/
	let product; //global variable

	 function checkProduct(multiply, multiplier) {


		return multiply * multiplier;
		console.log("The product of " + multiply + " and " + multiplier + " is ");
		
	}


	console.log(checkProduct(50,10));


/*
	function showProduct(checkProduct, multiply, multiplier) {

			checkProduct(multiply, multiplier);

	}

		showProduct(checkProduct,50,10);
*/

	//Quotient

	let quotient;

	function checkQuotient(divisor, dividend) {

		return divisor / dividend;
		console.log("The quotient of " + divisor + " and " + dividend + " is");
		console.log(quotient);
		
	}

	function showQuotient(checkQuotient, divisor, dividend) {

			checkQuotient(divisor, dividend);
	}

			showQuotient(checkQuotient, 50,10);
/*

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/

	let circleArea;

	function findArea(number) {

		return  (Math.PI * Math.pow(number,2)).toFixed(2);

		console.log("The result of getting the area of a circle with " + number + " radius:");
		console.log(circleArea);
		
	}

	function showArea(findArea, number) {

			findArea(number);
	}

	showArea(findArea,15);



/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	
*/

	let averageVar;


function checkAverage(a,b,c,d) {

		return (a+b+c+d) / 4;
		console.log("The average of " + " , " + a + " , " + b + " , " + c + " , " + d + " : ");
		console.log(averageVar);
		
	}

	function showAve(checkAverage,a,b,c,d){

		checkAverage(a,b,c,d);
	}

	showAve(checkAverage,20, 40, 60, 80); 


/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

	let	isPassingscore;


function checkScore(score, totals) {

		return (score / totals) * 100;
		console.log("Is " + score + " / " + totals + " a passing score?");

		return isPassingScore > 75;
		console.log(isPassingScore);
		
	}

		function showScore(checkScore, score, totals) {
			checkScore(score, totals);
		}

	showScore(checkScore,38, 50);

